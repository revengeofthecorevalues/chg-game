# CHG Hack Days Game

## Description
Currently, this is an HTML/JS game utilizing [Phaser.io](http://phaser.io/) and [Node.js](nodejs.org). Web sockets are used to communicate back and forth from the server and client to easily enable online multiplayer functionality.

## Node.js and NPM
You must have Node.js and NPM installed, which can be done [here](https://nodejs.org/en/).

## Project Setup
* Clone the repo
* `cd` into the directory
* run `npm install`
* run `npm run dev` to start the project

Once the project is started, you should be able to navigate to `localhost:3000` in your browser, and the game should display on the page.

## Game Info
The goal of the game is to be the first character to collect all 5 of CHG's core values. Once a player collects a value, they will begin to blink. The more values they have, the faster they blink. This acts as a visual cue to other players to see who has collected values. <br />
You can control player movement with the arrow keys, and hit other players with the space bar. Hitting another player will cause them to lose any values they've collected, as well as stun them for half a second. <br />
Core values that another player has lost will randomly spawn on the map again.