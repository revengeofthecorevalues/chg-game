const state = require('../state');

module.exports = {
  init(app) {
    const server = require('http').Server(app);
    const io = require('socket.io').listen(server);
  
    io.on('connection', socket => {
      console.log('A player connected:', socket.id);
  
      // Create a new player and add it to our players object
      const {name, x, y} = state.getCharacter();

      state.players[socket.id] = {
        playerId: socket.id,
        playerName: name,
        flipX: false,
        x,
        y,
        currentAnimation: null,
        attacking: false,
        isHit: false,
        coreValues: [],
      };

      const numPlayers = Object.keys(state.players).length;
      console.log(`There are ${numPlayers} players...`);
      if (numPlayers < state.requiredPlayers) {
        io.emit('waiting', state.requiredPlayers - numPlayers);
      }
      else {
        io.emit('startGame');
      }

      // Send players and register event listeners
      socket.on('gameStart', () => {
        // Send the players object to the players
        socket.emit('currentPlayers', state.players);
    
        // Update all other players of the new player
        // socket.broadcast.emit('newPlayer', state.players[socket.id]);
    
        // When a player moves, update the player data
        socket.on('playerMovement', movementData => {
          state.players[socket.id].x = movementData.x;
          state.players[socket.id].y = movementData.y;
          state.players[socket.id].flipX = movementData.flipX;
          state.players[socket.id].currentAnimation = movementData.currentAnimation;
    
          // Emit message to all players about the player that moved
          socket.broadcast.emit('playerMoved', state.players[socket.id]);
        });
  
        socket.on('attack', attackAnim => {
          state.players[socket.id].currentAnimation = attackAnim;
          state.players[socket.id].attacking = true;
          setTimeout(() => state.players[socket.id].attacking = false, 75);
          socket.broadcast.emit('attacking', state.players[socket.id]);
        });
  
        socket.on('hit', playerId => {
          if (state.players[playerId]) {
            state.clearValues(io, playerId);
            state.players[playerId].isHit = true;
            setTimeout(() => state.players[playerId].isHit = false, 500);
            io.emit('isHit', playerId);
          }
        });
  
        socket.on('valPickup', ({playerId, valueName}) => {
          state.players[playerId].coreValues.push(valueName);
          if (state.players[playerId].coreValues.length === state.values.length) {
            io.emit('gameOver', playerId);
          }
          else {
            io.emit('valPickedUp', {playerInfo: state.players[playerId], valueName});
          }
        });
  
        if (!state.intervalInit) {
          state.setValueInterval(io);
        }
      });

      // When a player disconnects, remove them from our players object
      socket.on('disconnect', () => {
        console.log('A player disconnected:', socket.id);
        state.clearValues(io, socket.id);
        delete state.players[socket.id];  
        io.emit('disconnect', socket.id);
        console.log(Object.keys(state.players).length, 'players remain...');
        if (!Object.keys(state.players).length) {
          state.reset();
        }
      });
    });
  
    return server;
  },
};