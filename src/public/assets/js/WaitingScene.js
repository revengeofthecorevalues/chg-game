class WaitingScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'WaitingScene',
    });
  }

  setMainText(text) {
    if (this.mainText) {
      this.mainText.setText(text);
      const x = Math.floor((this.game.config.width / 2) - (this.mainText.width / 2));
      const y = Math.floor((this.game.config.height / 2) - 200);
      this.mainText.setPosition(x, y);
    }
    else {
      this.mainText = this.add.text(0, 0, text, {fontSize: '32px'});
      const x = Math.floor((this.game.config.width / 2) - (this.mainText.width / 2));
      const y = Math.floor((this.game.config.height / 2) - 200);
      this.mainText.setPosition(x, y);
    }
  }

  updateMainText(waitingNum, txtType) {
    if (txtType === 'waiting') {
      this.setMainText(`WAITING FOR ${waitingNum} MORE PLAYERS TO JOIN`);
    }
    else {
      this.setMainText(`GAME STARTS IN ${waitingNum}...`);
    }
  }

  setInstructionText() {
    const text = [
      'Be the first player to collect all the core values!',
      'Players will flash if they\'ve collected any core values!',
      'Hit (space bar) other players to stun them and make them lose their values!',
      'Lost values will respawn on the map randomly!',
    ];
    let initialYPad = -100;
    text.forEach(t => {
      const txt = this.add.text(0, 0, t, {fontSize: '20px'});
      const x = Math.floor((this.game.config.width / 2) - (txt.width / 2));
      const y = Math.floor((this.game.config.height / 2) + (initialYPad));
      txt.setPosition(x, y);
      initialYPad += 50;
    });
  }

  create() {
    this.socket = io();
    console.log('In Waiting Scene...');

    this.socket.on('waiting', playersNeeded => {
      this.updateMainText(playersNeeded, 'waiting');
    });

    this.socket.on('startGame', () => {
      let countdown = 10;
      this.updateMainText(countdown);
      const itvl = setInterval(() => {
        countdown -= 1;
        this.updateMainText(countdown);

        if (!countdown) {
          clearInterval(itvl);
          this.scene.start('WorldScene', {socket: this.socket});
        }
      }, 1000);
      this.setInstructionText();
    });
  }
}