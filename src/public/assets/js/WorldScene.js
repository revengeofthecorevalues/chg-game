class WorldScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'WorldScene',
    });
  }

  init(data) {
    this.socket = data.socket;
  }

  create() {
    this.tintIntervals = {};
    this.tintMap = {
      0: 0,
      1: 500,
      2: 300,
      3: 150,
      4: 50,
    };

    this.otherPlayers = this.physics.add.group();
    this.values = this.physics.add.group();

    this.createMap();
    this.cursors = this.input.keyboard.createCursorKeys();

    // Add animations for all characters
    [
      'femaleAdventurer',
      'femalePerson',
      'maleAdventurer',
      'malePerson',
      'robot',
      'zombie',
    ].forEach(this.createAnimations.bind(this));

    // Listen for web socket events
    this.socket.on('currentPlayers', players => {
      this.createPlayer(players[this.socket.id]);
      Object.keys(players).forEach(id => {
        if (players[id].playerId !== this.socket.id) {
          this.addOtherPlayers(players[id]);
        }
      });
    });

    this.socket.on('newPlayer', playerInfo => {
      this.addOtherPlayers(playerInfo);
    });

    this.socket.on('disconnect', playerId => {
      this.otherPlayers.getChildren().forEach(player => {
        if (playerId === player.playerId) {
          this.clearTintInterval(player);
          player.destroy();
        }
      });
    });

    this.socket.on('playerMoved', playerInfo => {
      this.otherPlayers.getChildren().forEach(player => {
        if (playerInfo.playerId === player.playerId) {
          player.flipX = playerInfo.flipX;
          player.setPosition(playerInfo.x, playerInfo.y);
          if (!playerInfo.attacking && !playerInfo.isHit) {
            if (playerInfo.currentAnimation) {
              player.anims.play(playerInfo.currentAnimation, true);
            }
            else {
              player.anims.stop();
              player.setTexture(player.texture.key, 'idle');
            }
          }
        }
      });
    });

    this.socket.on('attacking', playerInfo => {
      this.otherPlayers.getChildren().forEach(player => {
        if (playerInfo.playerId === player.playerId) {
          player.anims.play(playerInfo.currentAnimation);
          setTimeout(() => player.setTexture(player.texture.key, 'idle'), 75);
        }
      });
    });

    this.socket.on('isHit', playerId => {
      if (playerId === this.socket.id) {
        this.isHit = true;
        this.clearTintInterval(this.player);
        this.player.setTexture(this.player.texture.key, 'hit');
        setTimeout(() => {
          this.isHit = false;
          this.player.setTexture(this.player.texture.key, 'idle');
        }, 500);
      }
      else {
        this.otherPlayers.getChildren().forEach(player => {
          if (player.playerId === playerId) {
            this.clearTintInterval(player);
            player.setTexture(player.texture.key, 'hit');
            setTimeout(() => player.setTexture(player.texture.key, 'idle'), 500);
          }
        });
      }
    });

    // TODO: Make load not outside of map
    this.socket.on('value', valueInfo => {
      console.log(valueInfo.valueName, valueInfo.x, valueInfo.y);
      let found = false;
      this.values.getChildren().forEach(val => {
        if (val.valueName === valueInfo.valueName) {
          val.setPosition(valueInfo.x, valueInfo.y);
          val.setActive(true).setVisible(true);
          found = true;
        }
      });

      if (!found) {
        const value = this.add.sprite(valueInfo.x, valueInfo.y, valueInfo.valueName);
        value.valueName = valueInfo.valueName;
        value.setScale(0.05);
        value.setSize(value.displayWidth, value.displayHeight);
        this.values.add(value);
        this.physics.add.overlap(this.container, value, this.onValOverlap, false, this);
      }
    });

    this.socket.on('valPickedUp', ({playerInfo, valueName}) => {
      this.values.getChildren().forEach(val => {
        if (val.valueName === valueName) {
          val.setVisible(false).setActive(false);
        }
      });

      let player = this.player;
      this.otherPlayers.getChildren().forEach(p => {
        if (p.playerId === playerInfo.playerId) {
          player = p;
        }
      });
      this.updateTintInterval(player, this.tintMap[playerInfo.coreValues.length]);
    });

    this.socket.on('gameOver', winnerId => {
      let data = {};
      if (winnerId === this.socket.id) {
        data.msg = 'YOU WON!!!';
      }
      else {
        data.msg = 'YOU LOST!!!';
      }
      this.socket.disconnect(true);
      this.scene.start('EndScene', data);
    });

    this.socket.emit('gameStart');
  }

  createMap() {
    // Create map
    this.map = this.make.tilemap({
      key: 'map',
    });

    // First parameter is the name of the tilemap in tiled
    const tiles = this.map.addTilesetImage('spritesheet', 'tiles', 16, 16, 1, 2);

    // Create the layers
    this.map.createStaticLayer('Grass', tiles, 0, 0);
    this.map.createStaticLayer('Obstacles', tiles, 0, 0);

    // Don't go out of the map
    this.physics.world.bounds.width = this.map.widthInPixels;
    this.physics.world.bounds.height = this.map.heightInPixels;
  }

  createAnimations(playerName) {
    this.anims.create({
      key: `${playerName}-left`,
      frames: this.anims.generateFrameNames(playerName, {
        prefix: 'walk',
        start: 0,
        end: 7,
      }),
      frameRate: 10,
      repeat: -1,
    });

    this.anims.create({
      key: `${playerName}-right`,
      frames: this.anims.generateFrameNames(playerName, {
        prefix: 'walk',
        start: 0,
        end: 7,
      }),
      frameRate: 10,
      repeat: -1,
    });

    this.anims.create({
      key: `${playerName}-up`,
      frames: this.anims.generateFrameNames(playerName, {
        prefix: 'walk',
        start: 0,
        end: 7,
      }),
      frameRate: 10,
      repeat: -1,
    });

    this.anims.create({
      key: `${playerName}-down`,
      frames: this.anims.generateFrameNames(playerName, {
        prefix: 'walk',
        start: 0,
        end: 7,
      }),
      frameRate: 10,
      repeat: -1,
    });

    this.anims.create({
      key: `${playerName}-attack`,
      frames: this.anims.generateFrameNames(playerName, {
        prefix: 'attack',
        start: 0,
        end: 0,
      }),
      frameRate: 20,
      repeat: 0,
    });
  }

  createPlayer(playerInfo) {
    console.log('player', playerInfo.x, playerInfo.y);
    // Our player sprite created through the physics system
    this.player = this.add.sprite(0, 0, playerInfo.playerName);
    this.player.playerId = playerInfo.playerId;
    this.player.setScale(0.2);

    this.container = this.add.container(playerInfo.x, playerInfo.y);
    this.container.setSize(16, 22);
    this.physics.world.enable(this.container);
    this.container.add(this.player);

    this.attacking = false;
    this.isHit = false;

    this.updateCamera();
    this.container.body.setCollideWorldBounds(true);
  }

  addOtherPlayers(playerInfo) {
    const otherPlayer = this.add.sprite(playerInfo.x, playerInfo.y, playerInfo.playerName);
    otherPlayer.playerId = playerInfo.playerId;
    otherPlayer.setScale(0.2);
    otherPlayer.setSize(16, 22);
    this.otherPlayers.add(otherPlayer);
    this.physics.add.overlap(this.container, otherPlayer, this.onMeetEnemy, false, this);
    this.updateTintInterval(otherPlayer, this.tintMap[playerInfo.coreValues.length]);
  }

  updateCamera() {
    // Limit camera to map
    this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
    this.cameras.main.startFollow(this.container);
    this.cameras.main.roundPixels = true; // avoid tile bleed
    this.cameras.main.zoomTo(3, 0);
  }

  onMeetEnemy(player, enemy) {
    if (this.attacking) {
      this.attacking = false;
      this.socket.emit('hit', enemy.playerId);
    }
  }

  onValOverlap(container, value) {
    if (value.visible) {
      value.setVisible(false).setActive(false);
      this.socket.emit('valPickup', {playerId: this.socket.id, valueName: value.valueName});
    }
  }

  updateTintInterval(player, ms) {
    if (!ms) {
      return;
    }

    const config = {
      delay: ms,
      callbackScope: this,
      loop: true,
      callback: () => player.setTint(Math.random() * 0xffffff),
    };

    if (this.tintIntervals[player.playerId]) {
      this.tintIntervals[player.playerId].reset(config);
    }
    else {
      this.tintIntervals[player.playerId] = this.time.addEvent(config);
    }
  }

  clearTintInterval(player) {
    if (this.tintIntervals[player.playerId]) {
      this.tintIntervals[player.playerId].remove();
      delete this.tintIntervals[player.playerId];
      player.clearTint();
    }
  }

  update() {
    if (this.container) {
      this.container.body.setVelocity(0);
      
      if (!this.isHit) {
        // Horizontal movement
        if (this.cursors.left.isDown) {
          this.container.body.setVelocityX(-80);
        }
        else if (this.cursors.right.isDown) {
          this.container.body.setVelocityX(80);
        }
  
        // Vertical movement
        if (this.cursors.up.isDown) {
          this.container.body.setVelocityY(-80);
        }
        else if (this.cursors.down.isDown) {
          this.container.body.setVelocityY(80);
        }
      }

      const namePrefix = `${this.player.texture.key}-`;

      // Update the animation last and give left/right animations precedence over up/down animations
      if (!this.attacking && !this.isHit) {
        if (this.cursors.left.isDown) {
          this.player.anims.play(`${namePrefix}left`, true);
          this.currentAnimation = `${namePrefix}left`;
          this.player.flipX = true;
        }
        else if (this.cursors.right.isDown) {
          this.player.anims.play(`${namePrefix}right`, true);
          this.currentAnimation = `${namePrefix}right`;
          this.player.flipX = false;
        }
        else if (this.cursors.up.isDown) {
          this.player.anims.play(`${namePrefix}up`, true);
          this.currentAnimation = `${namePrefix}up`;
        }
        else if (this.cursors.down.isDown) {
          this.player.anims.play(`${namePrefix}down`, true);
          this.currentAnimation = `${namePrefix}down`;
        }
        else {
          this.player.anims.stop();
          this.player.setTexture(this.player.texture.key, 'idle');
          this.currentAnimation = null;
        }

        if (Phaser.Input.Keyboard.JustDown(this.cursors.space)) {
          this.attacking = true;
          this.socket.emit('attack', `${namePrefix}attack`);
          this.player.anims.play(`${namePrefix}attack`);
          setTimeout(() => this.attacking = false, 75);
        }
      }

      const x = this.container.x;
      const y = this.container.y;
      const flipX = this.player.flipX;
      const currentAnimation = this.currentAnimation;

      if (
        this.oldPosition && (
        x !== this.oldPosition.x ||
        y !== this.oldPosition.y ||
        flipX !== this.oldPosition.flipX)
      ) {
        this.socket.emit('playerMovement', {x, y, flipX, currentAnimation});
      }

      // Save old position data
      this.oldPosition = {x, y, flipX};
    }
  }
}