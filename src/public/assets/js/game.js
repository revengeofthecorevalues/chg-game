const phaserConfig = {
  type: Phaser.AUTO,
  parent: 'content',
  width: 1200,
  height: 700,
  resolution: 2,
  pixelArt: true,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 0,
      },
      debug: false, // set to true to view zones
    },
  },
  scene: [
    BootScene,
    WaitingScene,
    WorldScene,
    EndScene,
  ],
};

const game = new Phaser.Game(phaserConfig);