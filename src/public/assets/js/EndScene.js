class EndScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'EndScene',
    });
  }

  setMainText(msg) {
    const text = this.add.text(0, 0, msg, {fontSize: '32px'});
    const x = Math.floor((this.game.config.width / 2) - (text.width / 2));
    const y = Math.floor((this.game.config.height / 2) - 200);
    text.setPosition(x, y);
  }

  setSubText() {
    const msg = 'Press "R" to play again!';
    const txt = this.add.text(0, 0, msg, {fontSize: '20px'});
    const x = Math.floor((this.game.config.width / 2) - (txt.width / 2));
    const y = Math.floor((this.game.config.height / 2) - 100);
    txt.setPosition(x, y);
  }

  init(data) {
    this.msg = data.msg;
  }

  create() {
    console.log('In End Scene...');

    this.replayKey = this.input.keyboard.addKey('R');
    this.setMainText(this.msg);
    this.setSubText();
  }

  update() {
    if (this.replayKey.isDown) {
      location.reload();
    }
  }
}