class BootScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'BootScene',
      active: true,
    });
  }

  preload() {
    // map tiles
    this.load.image('tiles', 'assets/map/spritesheet-extruded.png');

    // map in json format
    this.load.tilemapTiledJSON('map', 'assets/map/map.json');

    // Load images
    this.load.image('growth', 'assets/images/growth.png');
    this.load.image('improvement', 'assets/images/improvement.png');
    this.load.image('integrity', 'assets/images/integrity.png');
    this.load.image('peopleFirst', 'assets/images/people_first.png');
    this.load.image('quality', 'assets/images/quality.png');

    // Load texture altlas sprite sheets
    this.load.atlasXML('femaleAdventurer', 'assets/sprites/femaleAdventurer/character_femaleAdventurer_sheet.png', 'assets/sprites/femaleAdventurer/character_femaleAdventurer_sheet.xml');
    this.load.atlasXML('femalePerson', 'assets/sprites/femalePerson/character_femalePerson_sheet.png', 'assets/sprites/femalePerson/character_femalePerson_sheet.xml');
    this.load.atlasXML('maleAdventurer', 'assets/sprites/maleAdventurer/character_maleAdventurer_sheet.png', 'assets/sprites/maleAdventurer/character_maleAdventurer_sheet.xml');
    this.load.atlasXML('malePerson', 'assets/sprites/malePerson/character_malePerson_sheet.png', 'assets/sprites/malePerson/character_malePerson_sheet.xml');
    this.load.atlasXML('robot', 'assets/sprites/robot/character_robot_sheet.png', 'assets/sprites/robot/character_robot_sheet.xml');
    this.load.atlasXML('zombie', 'assets/sprites/zombie/character_zombie_sheet.png', 'assets/sprites/zombie/character_zombie_sheet.xml');
  }

  create() {
    this.scene.start('WaitingScene');
  }
}