module.exports = {
  players: {},

  requiredPlayers: 4,
  
  characters: [
    'femaleAdventurer',
    'femalePerson',
    'maleAdventurer',
    'malePerson',
    'robot',
    'zombie',
  ],

  values: [
    'growth',
    'improvement',
    'integrity',
    'peopleFirst',
    'quality',
  ],

  insertedCharacters: [],
  insertedValues: [],
  intervalInit: false,
  valueInterval: null,
  interval: 10000,
  intervalMin: 1500,

  getCharacter() {
    if (this.characters.length === this.insertedCharacters.length) {
      this.insertedCharacters = [];
    }

    const availableChars = this.characters.filter(c => !this.insertedCharacters.includes(c));
    const name = availableChars[Math.floor(Math.random() * availableChars.length)];
    this.insertedCharacters.push(name);
    return {
      name,
      x: Math.floor(Math.random() * 350) + 50,
      y: Math.floor(Math.random() * 400) + 50,
    };
  },

  getValue() {
    const availableVals = this.values.filter(v => !this.insertedValues.includes(v));
    const valueName = availableVals[Math.floor(Math.random() * availableVals.length)];
    this.insertedValues.push(valueName);
    return {
      valueName,
      x: Math.floor(Math.random() * 350) + 50,
      y: Math.floor(Math.random() * 400) + 50,
    };
  },

  clearValues(io, playerId) {
    this.players[playerId].coreValues.forEach(val => {
      const idx = this.insertedValues.indexOf(val);
      if (idx !== -1) {
        this.insertedValues.splice(idx, 1);
      }
    });
    this.players[playerId].coreValues = [];

    if (this.valueInterval) {
      clearInterval(this.valueInterval);
    }
    this.determineInterval();
    this.setValueInterval(io);
  },

  determineInterval() {
    if (this.interval === this.intervalMin) {
      return;
    }

    if (this.interval === 10000) {
      this.interval = 6000;
    }
    else if (this.interval - 1000 < this.intervalMin) {
      this.interval = this.intervalMin;
    }
    else {
      this.interval -= 1000;
    }
  },

  setValueInterval(io) {
    this.intervalInit = true;
    this.valueInterval = setInterval(() => {
      io.emit('value', this.getValue());
      if (this.values.length === this.insertedValues.length) {
        clearInterval(this.valueInterval);
        this.valueInterval = null;
      }
    }, this.interval);
  },

  reset() {
    if (this.valueInterval) {
      clearInterval(this.valueInterval);
    }
    this.insertedCharacters = [];
    this.insertedValues = [];
    this.valueInterval = null;
    this.intervalInit = false;
    this.interval = 10000;
  },
};